// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));


    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 768 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });

    $('.navbar a').click(function(){

        if($(window).width() < 768) {
            menu.removeAttr('style');
        }
    });
});

$('.navbar a').click(function(){

    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset: -60 });
    return false;
});


$(function($){
    var topnav = $('.topnav');
    var label = $('.label');
    $h = label.offset().top;

    $(window).scroll(function(){
        // Если прокрутили скролл ниже макушки блока, включаем фиксацию

        if ( $(window).scrollTop() > $h) {
            topnav.addClass('fix-top');
        }else{
            //Иначе возвращаем всё назад. Тут вы вносите свои данные
            topnav.removeClass('fix-top');
        }
    });
});

$('.review').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="review-nav prev"></span>',
    nextArrow: '<span class="review-nav next"></span>',
    responsive: [

        {
            breakpoint: 768,
            settings: {
                arrows: true
            }
        }
    ]
});



$(".btn-modal").fancybox({
    'padding'    : 0
});


$(".btn-modal-text").fancybox({
    'padding'    : 0,
    'maxWidth'   : 800
});


// tabs

$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});



$('.price-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.price');

    $(this).closest('.price-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.price-item').removeClass('active');
    box.find(tab).addClass('active');
});


ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
            center: [55.7434,37.8346],
            zoom: 15,
            controls: ['smallMapDefaultSet']
        });


    myMap.geoObjects
        .add(new ymaps.Placemark([55.7434,37.8346], {
            balloonContent: 'цвет <strong>воды пляжа бонди</strong>'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }));
}



$(document).ready(function() {

    $('.btn-send').click(function() {

        $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
        var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
        if(answer != false)
        {
            var $form = $(this).closest('form'),
                type   =     $('input[name="type"]', $form).val(),
                name    =     $('input[name="name"]', $form).val(),
                phone   =     $('input[name="phone"]', $form).val(),
                email   =     $('input[name="email"]', $form).val(),
                home   =     $('input[name="home"]', $form).val();
            console.log(type, name, phone, email, home);
            $.ajax({
                type: "POST",
                url: "form-handler.php",
                data: {type: type, name: name, phone: phone, email: email, home: home}
            }).done(function(msg) {
                $('form').find('input[type=text], textarea').val('');
                console.log('удачно');
                document.location.href = "./thankyou.html";
            });
        }
    });


});